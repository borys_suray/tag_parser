<?php

require_once 'ParserInterface.php';

/**
 * @author Borys Suray <surayborys@gmail.com>
 */

class Parser implements ParserInterface
{

	/**
	*	to grab tag from a html-page by requested url
	* 	@param string $url, string $tag 
	*	@return array
	*/
    public function process(string $url, string $tag):array
    {
        
    	#check if url begins with a protocol name. if not - add default http protocol
		if(!preg_match('~^((http|https|ftp):\/\/)~', $url, $matches))
		$url = 'http://' . $url;

		if($this->url_exist($url) == false)
			return ['url'=>'not exists'];

    	$html = file_get_contents($url);

    	#build regexp
    	$regExp = '#<' . $tag . '[^>]*>(?P<' . $tag . '>.*?)</' . $tag . '>#su';

    	return preg_match_all($regExp, $html, $matches) ? $matches[$tag] : ['matches'=>'not found'];
    	
    }

    /**
	*	to check if $url exists
	* 	@param string $url 
	*	@return bool
	*/
	protected function url_exist(string $url):bool
	{
		return (is_array(@get_headers($url, 1))) ? true : false;
	} 

}
